const express = require("express")
const todoController = require('../controller/todo.controller')

const app = express()
const router = express.Router()

router.get('/', todoController.getTodosController)

router.post('/', todoController.addTodoController)

router.delete('/:id', todoController.removeTodoController)

module.exports = router