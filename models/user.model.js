const Todo = require('./todo.model')
const mongoose = require('mongoose')

// module.exports = class User {
//     // username = {
//     //     data: "",
//     //     type: string
//     // }
//     // password = {
//     //     data: "",
//     //     type: string
//     // }
//     // todos = {
//     //     data: [],
//     //     type: Todo[]
//     // }

//     // constructor(username, password, todos) {
//     //     this.username.data = username
//     //     this.password.data = password
//     //     this.todos.data = todos
//     // }
// }

module.exports = mongoose.model("User", new mongoose.Schema({
    username: String,
    password: String,
    todos: []
}))
