const User = require('./user.model')
const mongoose = require('mongoose')

// module.exports = class Todo {
//   // job = {
//   //   data: "",
//   //   type: string
//   // }
//   // time = {
//   //   data: new Date.now(),
//   //   type: Date
//   // }
//   // user = {
//   //   data: null,
//   //   type: User
//   // }
//   // constructor(job, time, user) {
//   //   this.job.data = job
//   //   this.time.data = time,
//   //   this.user.data = user
//   // }
// }


module.exports = mongoose.model("Todo", new mongoose.Schema({
  task: String,
  time: Date,
  state: String
  // user: {}
}))
