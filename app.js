const Todo = require('./models/todo.model')
const User = require('./models/user.model')
const Express = require('express')
const app = Express();
const mongoose = require('mongoose')
const config = require('./config')
const todoRouter = require('./routers/todo.router')
const bodyParser = require('body-parser')
app.use(Express.static('./public'))
mongoose.connect(config.database, {useMongoClient: true})
mongoose.connection.on('connected', () => console.log('connected to db'))
app.use(bodyParser.json())

app.use('/api/v1/todo', todoRouter)

app.get('/', (req, res, next) => {
    res.sendFile('./public/index.html');
})

const server = app.listen(8000, () => {
    const host = server.address().address
    const port = server.address().port
    console.log(`app running on ${host}:${port}`)
})

