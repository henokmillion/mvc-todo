const todoService = require("../services/todo.service")

module.exports.getTodosController = (req, res, next) => {
    todoService.getTodos()
    .then(response =>res.status(response.status).json(response))
    .catch(err => res.status(err.stauts).json(err))
}

module.exports.addTodoController = (req, res, next) => {
    // console.log(req.body)
    const valid = !!req.body.todo
    if (valid) {
        todoService.addTodo(req.body.todo)
        .then(response => res.status(response.status).json(response))
        .catch(err => res.status(err.status).json(err))
    } else {
        res.status(401).json({status: 401, msg: "Error: Incomplete request body: invalid request"})
    }
}

module.exports.removeTodoController = (req, res, next) => {
    const valid = !!req.params.id
    if (valid) {
        todoService.removeTodoById(req.params.id)
        .then(response => res.status(response.status).json(response))
        .catch(err => res.status(err.status).json(err))        
    } else {
        response.status(400).json({
            status: 400,
            msg: `Incomplete request: param "id" is ${req.params.id}`
        })
    }
}