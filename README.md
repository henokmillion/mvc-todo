# mvc-todo
## description
This is a simple todo utility application that enables users to add, remove and 
view tasks or todos

## getting started
run `node app.js`
the server should start on `http://localhost:8000`

