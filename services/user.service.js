const Todo = requrie('../models/todo.model')
const User = require('../models/user.model')

module.exports.getUsers = () => {
    return new Promise((succeed, fail) => {
        User.find((err, users) => {
            if (err) {
                fail({
                    status: 500,
                    error: err
                })
            } else if (users) {
                succeed({
                    status: 200,
                    data: users
                })
            } else {
                fail({
                    status: 404,
                    msg: "No users found"
                })
            }
        })
    })
}

module.exports.addUser = (user) => {
    return new Promise((succeed, fail) => {
        const user = new User(user)
        user.save(err => {
            if (err) {
                fail({
                    status: 500,
                    error: err
                })
            } else {
                succeed({
                    status: 201,
                    msg: "successfully saved user"
                })
            }
        })
    })
}
