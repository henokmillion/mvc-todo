const Todo = require('../models/todo.model')
const User = require('../models/user.model')
const mongoose = require('mongoose')

module.exports.getTodos = () => {
    return new Promise((succeed, fail) => {
        Todo.find({ state: "active" }, (err, todos) => {
            if (err) {
                fail({

                })
            } else if (todos) {
                succeed({
                    status: 200,
                    data: todos
                })
            } else {
                fail({
                    status: 404,
                    msg: "No todos found"
                })
            }
        })
    })
}

module.exports.addTodo = (todo) => {
    return new Promise((succeed, fail) => {
        todo.state = "active"
        console.log(todo)
        todo.time = new Date(todo.time)
        const _todo = new Todo(todo)
        _todo.save(err => {
            if (err) {
                fail({
                    status: 500,
                    error: err
                })
            } else {
                succeed({
                    status: 201,
                    data: _todo,
                    msg: "successfully saved todo"
                })
            }
        })
    })
}

module.exports.removeTodoById = (todoId) => {
    return new Promise((succeed, fail) => {
        Todo.findOne({
            _id: mongoose.Types.ObjectId(todoId),
            state: "active"
        }, (err, todo) => {
            if (err) {
                fail({
                    status: 500,
                    msg: `Error: ${err}`
                })
            } else if (todo) {
                todo.state = "deleted"
                todo.save(_err => {
                    if (_err) {
                        fail({
                            status: 500,
                            msg: `Error: ${_err}`
                        })
                    } else {
                        succeed({
                            status: 200,
                            msg: `Successfully removed todo with id: ${todoId}`
                        })
                    }
                })
            } else {
                fail({
                    status: 404,
                    msg: `Error: No todo found with id: ${todoId}`
                })
            }
        })
    })
}
